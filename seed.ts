import { createConnection, Repository } from 'typeorm';
import { User, Team, Role, Members, WorkItem, Comment } from './data/entities';
import { UserRole } from './common/enums/user-role.enum';
import * as bcrypt from 'bcrypt';
import { connect } from 'net';
import { WorkItemStatus } from './common/enums/work-tem-status';
import { ReviewVotes } from './data/entities/review-votes';
// SENSITIVE DATA ALERT! - normally the admin credentials should not be present in the public repository, but for now we will roll with it - you can think about how to do it better
// run: `npm run seed` to seed the database

const seedRoles = async connection => {
    const rolesRepo: Repository<Role> = connection.manager.getRepository(Role);

    const roles: Role[] = await rolesRepo.find();
    if (roles.length) {
        console.log('The DB already has roles!');
        return;
    }

    const rolesSeeding: Array<Promise<Role>> = Object.keys(UserRole).map(
        async (roleName: string) => {
            const role: Role = rolesRepo.create({ name: roleName });
            return await rolesRepo.save(role);
        },
    );
    await Promise.all(rolesSeeding);

    console.log('Seeded roles successfully!');
};

const seedUsers = async connection => {
    const userRepo: Repository<User> = connection.manager.getRepository(User);
    const rolesRepo: Repository<Role> = connection.manager.getRepository(Role);
    const teamRepo: Repository<Team> = connection.manager.getRepository(Team);

    const admin = await userRepo.findOne({
        where: {
            name: 'admin',
        },
    });

    if (admin) {
        console.log('The DB already has an admin!');
        return;
    }

    const adminRole: Role = await rolesRepo.findOne({ name: UserRole.Admin });
    if (!adminRole) {
        console.log('The DB does not have an admin role!');
        return;
    }


    const newAdmin: User = userRepo.create({
        name: 'admin',
        email: 'admin@abv.bg',
        password: await bcrypt.hash('admin', 10),
        roles: [adminRole],
    });

    const basicRole: Role = await rolesRepo.findOne({ name: UserRole.Basic });
    if (!basicRole) {
        console.log('The DB does not have an basic role!');
        return;
    }

    const newBasic: User = userRepo.create({
        name: 'basic',
        email: 'basic@abv.bg',
        password: await bcrypt.hash('basic', 10),
        roles: [basicRole],
    });

    const newBasic1: User = userRepo.create({
        name: 'basic1',
        email: 'basic1@abv.bg',
        password: await bcrypt.hash('basic1', 10),
        roles: [basicRole],
    });

    const newBasic2: User = userRepo.create({
        name: 'basic2',
        email: 'basic2@abv.bg',
        password: await bcrypt.hash('basic2', 10),
        roles: [basicRole],
    });

    await userRepo.save(newBasic);
    await userRepo.save(newBasic1);
    await userRepo.save(newBasic2);
    await userRepo.save(newAdmin);
    console.log('Seeded users successfully!');
};

const seedTeams = async connection => {
    const userRepo: Repository<User> = connection.manager.getRepository(User);
    const teamRepo: Repository<Team> = connection.manager.getRepository(Team);

    const admin = await userRepo.findOne({ name: 'admin' });
    const basic = await userRepo.findOne({ name: 'basic' });

    const team1 = new Team();
    team1.name = 'FrontEnd';
    team1.author = Promise.resolve(admin);

    const team2 = new Team();
    team2.name = 'BackEnd';
    team2.author = Promise.resolve(basic);

    await teamRepo.save(team1);
    await teamRepo.save(team2);

    console.log('Seeded teams successfully!');
};

const seedMembers = async connection => {
    const userRepo: Repository<User> = connection.manager.getRepository(User);
    const teamRepo: Repository<Team> = connection.manager.getRepository(Team);
    const membersRepo: Repository<Members> = connection.manager.getRepository(Members);

    const admin = await userRepo.findOne({ name: 'admin' });
    const basic = await userRepo.findOne({ name: 'basic' });
    const basic1 = await userRepo.findOne({ name: 'basic1' });

    const frontEndTeam = await teamRepo.findOne({ name: 'FrontEnd' });
    const backEndTeam = await teamRepo.findOne({ name: 'BackEnd' });

    const teamMembers0 = new Members();
    teamMembers0.team = Promise.resolve(frontEndTeam);
    teamMembers0.member = Promise.resolve(admin);
    teamMembers0.isInTeam = false;

    const teamMembers1 = new Members();
    teamMembers1.team = Promise.resolve(frontEndTeam);
    teamMembers1.member = Promise.resolve(basic);
    teamMembers1.isInTeam = true;

    const teamMembers2 = new Members();
    teamMembers2.team = Promise.resolve(backEndTeam);
    teamMembers2.member = Promise.resolve(admin);
    teamMembers2.isInTeam = true;

    const teamMembers3 = new Members();
    teamMembers3.team = Promise.resolve(backEndTeam);
    teamMembers3.member = Promise.resolve(basic);
    teamMembers3.isInTeam = true;

    const teamMembers4 = new Members();
    teamMembers4.team = Promise.resolve(backEndTeam);
    teamMembers4.member = Promise.resolve(basic1);
    teamMembers4.isInTeam = false;

    await membersRepo.save(teamMembers0);
    await membersRepo.save(teamMembers1);
    await membersRepo.save(teamMembers2);
    await membersRepo.save(teamMembers3);
    await membersRepo.save(teamMembers4);

    console.log('Seeded team members successfully!');
};

const seedWorkItems = async connection => {
    const userRepo: Repository<User> = connection.manager.getRepository(User);
    const workItemRepo: Repository<WorkItem> = connection.manager.getRepository(WorkItem);

    const workItem1Assignee = await userRepo.findOne({ name: 'admin' });
    const workItem2Assignee = await userRepo.findOne({ name: 'basic' });

    const reviewer1 = await userRepo.findOne({ name: 'admin' });
    const reviewer2 = await userRepo.findOne({ name: 'basic2' });

    const workItem1 = new WorkItem();
    workItem1.assignee = Promise.resolve(workItem1Assignee);
    workItem1.title = 'testTitle';
    workItem1.description = 'this is the body of the workItem';
    workItem1.tags = 'random tags';
    workItem1.status = WorkItemStatus.Pending;
    workItem1.reviewers = Promise.resolve([reviewer1, reviewer2]);

    const workItem2 = new WorkItem();
    workItem2.assignee = Promise.resolve(workItem2Assignee);
    workItem2.title = 'basic workTitle';
    workItem2.description = 'basic workbody and description';
    workItem2.tags = 'basic tags';
    workItem2.status = WorkItemStatus.Pending;
    workItem2.reviewers = Promise.resolve([reviewer1]);

    await workItemRepo.save(workItem1);
    await workItemRepo.save(workItem2);
    console.log('Seeded workItems successfully!');
};

const seedComments = async connection => {
    const userRepo: Repository<User> = connection.manager.getRepository(User);
    const workItemRepo: Repository<WorkItem> = connection.manager.getRepository(WorkItem);
    const commentRepo: Repository<Comment> = connection.manager.getRepository(Comment);

    const comment1Author = await userRepo.findOne({ name: 'basic2' });
    const comment2Author = await userRepo.findOne({ name: 'admin' });
    const comment1WorkItem = await workItemRepo.findOne({ title: 'the best title' });

    const comment1 = new Comment();
    comment1.author = Promise.resolve(comment1Author);
    comment1.title = 'basic comment title';
    comment1.description = 'basic comment bdy';
    comment1.workItem = Promise.resolve(comment1WorkItem);

    const comment2 = new Comment();
    comment2.author = Promise.resolve(comment2Author);
    comment2.title = 'admi title';
    comment2.description = 'admin was here';
    comment2.workItem = Promise.resolve(comment1WorkItem);

    await commentRepo.save(comment1);
    await commentRepo.save(comment2);

    console.log('Seeded comments successfully!');
};

const seedReviewVotes = async connection => {
    const userRepo: Repository<User> = connection.manager.getRepository(User);
    const workItemRepo: Repository<WorkItem> = connection.manager.getRepository(WorkItem);
    const reviewVotesRepo: Repository<ReviewVotes> = connection.manager.getRepository(ReviewVotes);

    const admin = await userRepo.findOne({ name: 'admin' });
    const basic = await userRepo.findOne({ name: 'basic' });
    const workItem = await workItemRepo.findOne({ title: 'testTitle' });

    const reviewVote = new ReviewVotes();
    reviewVote.state = WorkItemStatus.Accepted;
    reviewVote.author = Promise.resolve(admin);
    reviewVote.workItem = Promise.resolve(workItem);

    const reviewVote2 = new ReviewVotes();
    reviewVote2.state = WorkItemStatus.Rejected;
    reviewVote2.author = Promise.resolve(basic);
    reviewVote2.workItem = Promise.resolve(workItem);

    await reviewVotesRepo.save(reviewVote);
    await reviewVotesRepo.save(reviewVote2);

    console.log('Seeded reviewVotes successfully!');
};

const seed = async () => {
    console.log('Seed started!');
    const connection = await createConnection();

    await seedRoles(connection);
    await seedUsers(connection);
    await seedTeams(connection);
    await seedMembers(connection);
    await seedWorkItems(connection);
    await seedComments(connection);
    await seedReviewVotes(connection);

    await connection.close();
    console.log('Seed completed!');
};

seed().catch(console.error);
