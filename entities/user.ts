import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    OneToMany,
    JoinTable,
    ManyToMany,
    CreateDateColumn,
    OneToOne,
    JoinColumn,
    ManyToOne,
} from "typeorm";
import { Team } from "./team";
import { WorkItem } from "./work-item";
import { Comment } from "./comment";
import { Role } from "./role";
import { Members } from "./members";
import { ReviewVotes } from "./review-votes";
import { ReviewRequest } from "./review-request";
@Entity('users')
export class User {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    name: string;

    @Column({ unique: true })
    email: string;

    @Column()
    password: string;

    @CreateDateColumn()
    createdOn: Date;

    @OneToMany(type => ReviewRequest, reviewRequest => reviewRequest.author)
    reviewRequests: Promise<ReviewRequest[]>;

    @OneToMany(type => Team, team => team.author)
    createdTeam: Promise<Team[]>;

    @OneToMany(type => Members, members => members.member)
    teams: Promise<Team[]>;

    @OneToMany(type => WorkItem, workItem => workItem.assignee)
    workItems: Promise<WorkItem[]>;

    @OneToMany(type => Comment, comment => comment.author)
    comments: Promise<Comment[]>;

    @OneToMany(type => ReviewVotes, reviewVotes => reviewVotes.author)
    reviewVotes: Promise<ReviewVotes[]>;

    @ManyToMany(type => Role, { eager: true })
    @JoinTable()
    roles: Role[];
}
