import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    OneToOne,
    JoinColumn,
    OneToMany,
    ManyToOne,
    CreateDateColumn,
} from 'typeorm';
import { User } from './user';
import { Members } from './members';

@Entity('teams')
export class Team {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    name: string;

    @CreateDateColumn()
    createdOn: Date;

    @ManyToOne(type => User, user => user.createdTeam)
    author: Promise<User>;

    @OneToMany(type => Members, members => members.team)
    members: Promise<User[]>;
}
